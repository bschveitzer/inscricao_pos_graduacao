import VueRouter from 'vue-router';
import Login from '../views/login/Login.vue';
import Home from '../views/home/Home.vue';
import AdminScreen from '../views/adminScreen/AdminScreen.vue';
import CacheValues from '../utils/CacheValues';
const configs = require('../configs/RouterConfig.json');
const initial_routes = [
  { path: '/', component: Login}
];
const loged_routes = [
  {path: '/administracao', component: AdminScreen},
  {path: '/home', component: Home, user_type: 'doctorate'},
  {path: '/home', component: Home, user_type: 'master'},
];
import SIOM from '../events/SIOM';

class Routes {
  constructor(configs) {
    configs.routes = Routes.add_init_routes(configs.routes);
    this._vueRouter = new VueRouter(configs);
    this._listeners = {
      'change_route': this.change_route.bind(this),
      'user_logged': this.user_logged.bind(this),
    };
    this.wiring();
  }

  get listeners() {
    return this._listeners;
  }

  static add_init_routes(routes) {
    return routes.concat(initial_routes);
  }

  get router() {
    return this._vueRouter;
  }

  add_routes(routes) {
    this.router.addRoutes(routes);
    this.change_route_no_historic(routes[0].path);
    routes[0].component.methods['ready']();
  }

  change_route_no_historic(route) {
    this.router.replace({path: route});
  }

  user_logged(user) {
    this.add_routes(Routes.get_routes_by_type(user.type));
    CacheValues.updateData('user', user);
    SIOM.send_to_browser('user_ready');
  }

  static get_routes_by_type(type) {
    let ret = [];
    for (let i = 0; i < loged_routes.length; i++) {
      if (loged_routes[i].user_type === type) {
        ret.push(loged_routes[i]);
      }
    }
    return ret;
  }

  change_route(route) {
    for(let index in loged_routes){
      if(loged_routes.hasOwnProperty(index) && loged_routes[index].path === route){
        loged_routes[index].component.methods.ready();
      }
    }
    this.router.push({path: route});
  }

  wiring() {
    for (let event in this.listeners) {
      if (this.listeners.hasOwnProperty(event)) {
        SIOM.on(event, this.listeners[event]);
      }
    }
  }
}

export default new Routes(configs);