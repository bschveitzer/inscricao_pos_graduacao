import Basic from '../../utils/BasicComponents';
import CacheValues from '../../utils/CacheValues';

class Home extends Basic{
  constructor(){
    super('Home');

    this.methods = {
      'change_bar': this.change_bar.bind(this),
    };

    this.data = {
      user: CacheValues.getData('user'),
      login:{
        email: '',
        password: ''
      },
      validate: {
        email: false,
        password: false
      },
      active_bar: 'primary'
    };
    this.wiring();
  }

  change_bar(new_bar){
    this.data.active_bar = new_bar;
  }
}
export default new Home().$vue;