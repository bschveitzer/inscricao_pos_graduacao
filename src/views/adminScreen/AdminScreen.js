import Basic from '../../utils/BasicComponents';
import CacheValues from '../../utils/CacheValues';
import EditSubject from '../../componentes/editSubject/editSubject.vue';
import SucessDialog from '../../componentes/sucessDialog/successDialog.vue';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';
import RemoveSubject from '../../componentes/removeSubject/removeSubject.vue';
import RemoveArea from '../../componentes/removeArea/removeArea.vue';
import ReactivateArea from '../../componentes/reactivateArea/reactivateArea.vue';
import EditArea from '../../componentes/editArea/editArea.vue';

class AdminScreen extends Basic {
  constructor() {
    super('AdminScreen');
    this.methods = {
      'change_bar': this.change_bar.bind(this),
      'ready': this.ready.bind(this),
      'edit_subject': this.edit_subject.bind(this),
      'edit_area': this.edit_area.bind(this),
      'remove_subject': this.remove_subject.bind(this),
      'remove_area': this.remove_area.bind(this),
      'reactivate_area': this.reactivate_area.bind(this)
    };

    this.data = {
      user: CacheValues.getData('user'),
      login:{
        email: '',
        password: ''
      },
      validate: {
        email: false,
        password: false
      },
      active_bar: 'primary',
      subjects: [],
      active_areas: [],
      disabled_areas: [],
      validate_subjects: [],
      pey:'',
      active_table: true,
      removed_table : false
    };

    this.components = {
      'EditSubject': EditSubject,
      'successDialog': SucessDialog,
      'loadingDialog': LoadingDialog,
      'RemoveSubject': RemoveSubject,
      'RemoveArea': RemoveArea,
      'ReactivateArea': ReactivateArea,
      'EditArea': EditArea
    };

    this.listeners = {
      'return_subject_read': this.return_subject_read.bind(this),
      'return_area_read': this.return_area_read.bind(this),
      'return_course_read': this.return_course_read.bind(this),
      'return_subject_update': this.return_edit_subject.bind(this),
      'return_subject_read_after_edit': this.return_subject_read_after_edit.bind(this),
      'return_subject_remove': this.return_subject_remove.bind(this),
      'return_area_remove': this.return_area_remove.bind(this),
      'return_area_reactivate': this.return_area_reactivate.bind(this),
      'return_area_update': this.return_area_update.bind(this)
    };

    this.wiring();
  }

  change_bar(new_bar){
    this.data.active_bar = new_bar;
  }

  ready(){
    this.send_to_server('subject_read', {}, 'return_subject_read');
    this.send_to_server('area_read', {}, 'return_area_read');
    this.send_to_server('course_read', {}, 'return_course_read');
  }

  return_subject_read(msg){
    if(msg.source !== this) return;
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }
    this.data.subjects = msg.datas.data;
  }

  return_area_read(msg){
    if(msg.source !== this) return;
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    this.data.active_areas = msg.datas.data.active;
    this.data.disabled_areas = msg.datas.data.deactive;
  }

  return_course_read(msg){
    console.log('LEU OS CURSOS', msg);
  }

  edit_subject(subject){
    this.components['EditSubject'].methods.openDialog(subject, this.siom, this);
  }

  return_edit_subject(msg){
    if(msg.source !== this) return;
    if(!msg.datas.success){
      this.data.subjects = this.data.validate_subjects;
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    this.send_to_server('subject_read', {}, 'return_subject_read_after_edit');
    this.send_to_server('area_read', {}, 'return_area_read');
  }

  return_subject_read_after_edit(msg){
    if(msg.source !== this) return;
    this.components['loadingDialog'].methods.closeDialog();
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    this.data.subjects = msg.datas.data;

    this.components['successDialog'].methods.openDialog('Disciplina editada com sucesso!', null, null);
  }

  remove_subject(subject){
    this.components['RemoveSubject'].methods.openDialog(subject, this.siom, this);
  }

  return_subject_remove(msg){
    if(msg.source !== this) return;
    this.components['loadingDialog'].methods.closeDialog();
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    for(let index = 0; index < this.data.subjects.length; index++){
      if(this.data.subjects[index].id === msg.datas.data[0].id){
        this.data.subjects.splice(index, 1);
      }
    }

    this.send_to_server('area_read', {}, 'return_area_read');
    this.components['successDialog'].methods.openDialog('Disciplina removida com sucesso!', null, null);
  }

  edit_area(area){
    this.components['EditArea'].methods.openDialog(area, this.data.subjects, this.siom, this);
  }

  return_area_update(msg){
    if(msg.source !== this) return;
    this.components['loadingDialog'].methods.closeDialog();
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    this.components['successDialog'].methods.openDialog('Área editada com sucesso!', null, null);
  }

  remove_area(area){
    this.components['RemoveArea'].methods.openDialog(area, this.siom, this);
  }

  return_area_remove(msg){
    if(msg.source !== this) return;
    this.components['loadingDialog'].methods.closeDialog();
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    for(let index = 0; index < this.data.active_areas.length; index++){
      if(this.data.active_areas[index].id === msg.datas.data[0].id){
        this.data.active_areas.splice(index, 1);
      }
    }

    this.data.disabled_areas.push(msg.datas.data[0]);

    this.send_to_server('course_read', {}, 'return_course_read');
    this.components['successDialog'].methods.openDialog('Área removida com sucesso!', null, null);
  }

  reactivate_area(area){
    this.components['ReactivateArea'].methods.openDialog(area, this.siom, this);
  }

  return_area_reactivate(msg){
    if(msg.source !== this) return;
    this.components['loadingDialog'].methods.closeDialog();
    if(!msg.datas.success){
      return this.components_controller.BasicAlert.open(msg.datas.data);
    }

    for(let index = 0; index < this.data.disabled_areas.length; index++){
      if(this.data.disabled_areas[index].id === msg.datas.data[0].id){
        this.data.disabled_areas.splice(index, 1);
      }
    }

    this.data.active_areas.push(msg.datas.data[0]);

    this.send_to_server('course_read', {}, 'return_course_read');
    this.components['successDialog'].methods.openDialog('Área reativada com sucesso!', null, null);
  }
}

export default new AdminScreen().$vue;