import Basic from '../../utils/BasicComponents';
import CacheValues from '../../utils/CacheValues';
import BasicAlert from "../../componentes/utilsComponents/BasicAlert.vue";
import AdminScreen from "../../views/adminScreen/AdminScreen.vue";
import Home from "../../views/home/Home.vue";

class Sidenav extends Basic{
  constructor(){
    super('Sidenav');
    this.methods = {
      'go': this.go.bind(this),
      'logar': this.logar.bind(this),
      'change_bar': this.change_bar.bind(this)
    };
    this.listeners = {
      'retorno_login': this.retorno_login.bind(this),
      'user_ready': this.set_process_dates.bind(this),
    };
    this.components = {
      BasicAlert: BasicAlert,
      AdminScreen: AdminScreen,
      Home: Home
    };
    this.data = {
      user: CacheValues.getData('user'),
      login:{
        email: '',
        password: ''
      },
      validate: {
        email: false,
        password: false
      },
      active_bar: 'primary',
      initial_date_validate: null,
      final_date_validate: null
    };
    this.wiring();
  }
  go(route){
    this.$instance.$router.push({path: route});
  }

  retorno_login(msg){
    this.data.login.email = '';
    this.data.login.password = '';
    if(msg.source !== this) return;
    if(!msg.datas.success){
     if(msg.datas.data.title === "Senha incorreta."){
        return this.data.validate.password = true;
     }else if(msg.datas.data.title === "Usuário não cadastrado."){
       return this.data.validate.email = true;
     }else{
       return this.components_controller.BasicAlert.open(msg.datas.data);
     }
    }
    this.send_to_browser('user_logged', msg.datas.data)
  }
  logar(){
    this.data.validate.email = false;
    this.data.validate.password = false;
    this.send_to_server('logar', this.data.login, 'retorno_login');
  }

  change_bar(new_bar){
    this.data.active_bar = new_bar;
    if(!this.data.user.type){
      this.components.AdminScreen.methods.change_bar(new_bar);
    }else{
      this.components.Home.methods.change_bar(new_bar);
    }
  }

  set_process_dates(){
    if(this.data.user.period){
      this.data.initial_date_validate = this.data.user.period.initialDate;
      this.data.final_date_validate = this.data.user.period.finalDate;
    }
  }
}
export default new Sidenav().$vue;