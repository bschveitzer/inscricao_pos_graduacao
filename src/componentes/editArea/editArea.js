import Basic from '../../utils/BasicComponents';
import Mensagem from '../../events/Mensagem';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';

class EditArea extends Basic {
  constructor() {
    super('EditArea');
    this.methods = {
      openDialog: this.openDialog.bind(this),
      closeDialog: this.closeDialog.bind(this),
      edit_area: this.edit_area.bind(this),
      add_subject: this.add_subject.bind(this),
      remove_subject: this.remove_subject.bind(this),
    };
    this.data = {
      active: false,
      area: {},
      subjects: [],
      validate: {},
      validate_subjects: [],
      siom: null,
      source: null,
      new_subjects: [],
      primary: true
    };

    this.components = {
      'loadingDialog': LoadingDialog
    };

    this.wiring();
  }


  openDialog(area, subjects, siom, source) {
    this.data.area = area;
    this.data.validate.label = area.label;
    this.data.validate.description = area.description;

    for (let index = 0; index < area.subjects.length; index++) {
      this.data.validate_subjects.push(area.subjects[index]);
    }

    this.data.new_subjects = area.subjects;
    this.data.subjects = subjects;
    this.data.siom = siom;
    this.data.source = source;
    this.data.active = true;

  }

  closeDialog() {
    this.data.active = false;
    this.data.area.label = this.data.validate.label;
    this.data.area.description = this.data.validate.description;
    this.data.area.subjects = this.data.validate_subjects;
    this.data.validate = {};
    this.data.validate_subjects = [];
    this.data.primary = true;
  }

  edit_area() {

    let subjects_id = [];

    for (let index = 0; index < this.data.new_subjects.length; index++) {
      subjects_id.push(this.data.new_subjects[index].id);
    }

    let update_area = {
      id: this.data.area.id,
      update: {
        label: this.data.area.label,
        description: this.data.area.description,
        subjects: subjects_id
      }
    };

    let msg = new Mensagem('area_update', update_area, 'return_area_update', this.data.source);
    this.data.siom.send_to_server(msg);
    this.data.active = false;
    this.components['loadingDialog'].methods.openDialog();

  }

  add_subject(item) {
    this.data.new_subjects.push(item);

    for (let index = 0; index < this.data.subjects.length; index++) {
      if (this.data.subjects[index].id === item.id) {
        this.data.subjects.splice(index, 1);
      }
    }
  }

  remove_subject(item) {
    this.data.subjects.push(item);

    for (let index = 0; index < this.data.new_subjects.length; index++) {
      if (this.data.new_subjects[index].id === item.id) {
        this.data.new_subjects.splice(index, 1);
      }
    }
  }
}

export default new EditArea().$vue;