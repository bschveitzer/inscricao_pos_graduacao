import Basic from '../../utils/BasicComponents';
class BasicAlert extends Basic {
  constructor(){
    super('Basic');
    this.methods = {
      callMethod: this.callMethod.bind(this)
    };
    this.data = {
      modal: {
        title: '',
        description: '',
        buttons: [],
      }
    };
    this.wiring();
  }
  open(data) {
    this.data.modal = data;
    this.$instance.$refs['basicAlert'].open();
  }
  closeDialog() {
    this.$instance.$refs['basicAlert'].close();
  }
  teste(){
    console.log('deu boa aqui, babaca');
  }
  teste_outro(){
    for(let i = 1; i < 11; i++){
      console.log(i, 'A');
    }
    this.closeDialog();
  }
  callMethod(method){
    console.log('o nome do metodo é ', method);
    console.log('descricao do metodo', this[method]);
    if(!method){
      this.closeDialog();
    } else {
      this[method]();
    }
  }
}
export default new BasicAlert().$vue;