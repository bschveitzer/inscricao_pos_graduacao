import Basic from '../../utils/BasicComponents';
import Mensagem from '../../events/Mensagem';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';

class ReactivateArea extends Basic {
  constructor() {
    super('ReactivateArea');
    this.methods = {
      openDialog: this.openDialog.bind(this),
      closeDialog: this.closeDialog.bind(this),
      reactivateArea: this.reactivateArea.bind(this)
    };
    this.data = {
      active: false,
      area: {},
      validate: {},
      siom: null,
      source: null
    };

    this.components = {
      'loadingDialog': LoadingDialog
    };

    this.wiring();
  }


  openDialog(area, siom, source) {
    this.data.area = area;
    this.data.siom = siom;
    this.data.source = source;
    this.data.active = true;

  }

  closeDialog() {
    this.data.active = false;
  }

  reactivateArea() {
    let reactivate_area = {
      id: this.data.area.id,
      update: {
        removed: false,
      }
    };

    let msg = new Mensagem('area_update', reactivate_area, 'return_area_reactivate', this.data.source);
    this.data.siom.send_to_server(msg);
    this.data.active = false;
    this.components['loadingDialog'].methods.openDialog();

  }
}

export default new ReactivateArea().$vue;