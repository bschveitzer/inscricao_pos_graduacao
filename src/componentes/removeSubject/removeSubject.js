import Basic from '../../utils/BasicComponents';
import Mensagem from '../../events/Mensagem';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';

class RemoveSubject extends Basic {
  constructor() {
    super('RemoveSubject');
    this.methods = {
      openDialog: this.openDialog.bind(this),
      closeDialog: this.closeDialog.bind(this),
      removeSubject: this.removeSubject.bind(this)
    };
    this.data = {
      active: false,
      subject: {},
      validate: {},
      siom: null,
      source: null
    };

    this.components = {
      'loadingDialog': LoadingDialog
    };

    this.wiring();
  }


  openDialog(subject, siom, source) {
    this.data.subject = subject;
    this.data.siom = siom;
    this.data.source = source;
    this.data.active = true;

  }

  closeDialog() {
    this.data.active = false;
  }

  removeSubject() {
    let remove_subject = {
      id: this.data.subject.id,
    };

    let msg = new Mensagem('subject_remove', remove_subject, 'return_subject_remove', this.data.source);
    this.data.siom.send_to_server(msg);
    this.data.active = false;
    this.components['loadingDialog'].methods.openDialog();

  }
}

export default new RemoveSubject().$vue;