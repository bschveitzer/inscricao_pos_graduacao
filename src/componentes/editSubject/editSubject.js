import Basic from '../../utils/BasicComponents';
import Mensagem from '../../events/Mensagem';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';

class EditSubject extends Basic {
  constructor(){
    super('EditSubject');
    this.methods = {
      openDialog: this.openDialog.bind(this),
      closeDialog: this.closeDialog.bind(this),
      editSubject: this.editSubject.bind(this)
    };
    this.data = {
      active: false,
      subject: {},
      validate: {},
      siom: null,
      source: null
    };

    this.components = {
      'loadingDialog': LoadingDialog
    };

    this.wiring();
  }


  openDialog(subject, siom, source) {
    this.data.validate.label = subject.label;
    this.data.validate.code = subject.code;
    this.data.subject = subject;
    this.data.siom = siom;
    this.data.source = source;
    this.data.active = true;

  }
  closeDialog() {
    this.data.active = false;
    this.data.subject.label = this.data.validate.label;
    this.data.subject.code = this.data.validate.code;
  }

  editSubject(){
    if((this.data.validate.label !== this.data.subject.label ||
        this.data.validate.code !== this.data.subject.code) &&
      this.data.subject.label.length > 0){

      let update_subject = {
        id: this.data.subject.id,
        update: {
          code: this.data.subject.code,
          label: this.data.subject.label,
        }
      };


      let msg = new Mensagem('subject_update', update_subject, 'return_subject_update', this.data.source);
      this.data.siom.send_to_server(msg);
      this.data.active = false;
      this.components['loadingDialog'].methods.openDialog();
    }
  }
}
export default new EditSubject().$vue;