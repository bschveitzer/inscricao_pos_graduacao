import Basic from '../../utils/BasicComponents';

class SuccessDialog extends Basic {
  constructor(){
    super('successDialog');
    this.data = {
      message: '',
      first_line: '',
      second_line: '',
      active: false
    };
    this.listeners = {};
    this.methods = {
      'openDialog': this.openDialog.bind(this),
      'closeDialog': this.closeDialog.bind(this),
    };
    this.wiring();
  }
  openDialog(main_message, first_line, second_line) {
    this.data.message = main_message;
    this.data.first_line = first_line;
    this.data.second_line = second_line;
    this.data.active = true;
  }
  closeDialog() {
    this.data.message = '';
    this.data.first_line = '';
    this.data.second_line = '';
    this.data.active = false;
  }
}
export default new SuccessDialog().$vue;