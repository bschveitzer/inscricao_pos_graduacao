import Basic from '../../utils/BasicComponents';
import Mensagem from '../../events/Mensagem';
import LoadingDialog from '../../componentes/loadingDialog/loadingDialog.vue';

class RemoveArea extends Basic {
  constructor() {
    super('RemoveArea');
    this.methods = {
      openDialog: this.openDialog.bind(this),
      closeDialog: this.closeDialog.bind(this),
      removeArea: this.removeArea.bind(this)
    };
    this.data = {
      active: false,
      area: {},
      validate: {},
      siom: null,
      source: null
    };

    this.components = {
      'loadingDialog': LoadingDialog
    };

    this.wiring();
  }


  openDialog(area, siom, source) {
    this.data.area = area;
    this.data.siom = siom;
    this.data.source = source;
    this.data.active = true;

  }

  closeDialog() {
    this.data.active = false;
  }

  removeArea() {
    let remove_area = {
      id: this.data.area.id,
      update: {
        removed: true,
      }
    };

    let msg = new Mensagem('area_update', remove_area, 'return_area_remove', this.data.source);
    this.data.siom.send_to_server(msg);
    this.data.active = false;
    this.components['loadingDialog'].methods.openDialog();

  }
}

export default new RemoveArea().$vue;