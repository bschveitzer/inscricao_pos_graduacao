import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Router from './routes/Routes';
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css';
import VeeValidate from 'vee-validate'


Vue.use(VeeValidate);
Vue.use(VueRouter);
Vue.use(VueMaterial);

const dictionary = {
  pt: {
      custom: {
        email:{
          email: 'Insira um e-mail válido'
        },
        password: {
          min: 'A senha deve ter de 6 a 12 dígitos',
          max: 'A senha deve ter de 6 a 12 dígitos'
        }
      }
  }
};

VeeValidate.Validator.updateDictionary(dictionary);
VeeValidate.Validator.setLocale('pt');

let router = Router.router;
const app = new Vue({
  router,
  render: h => h(App)
}).$mount('#container');